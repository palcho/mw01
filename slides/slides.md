---
title: Exercício 01 de Middleware
author:
- Felipe Guerra
- Tiago Figueiredo
---

# Resultados UDP 
```
1 cliente : m = 0.037ms, dp = 0.005ms 
2 clientes: m = 0.034ms, dp = 0.014ms 
3 clientes: m = 0.049ms, dp = 0.013ms 
4 clientes: m = 0.061ms, dp = 0.025ms 
5 clientes: m = 0.076ms, dp = 0.026ms
.
```
# Resultados TCP
```
1 cliente : m = 0.045ms, dp = 0.019ms 
2 clientes: m = 0.035ms, dp = 0.009ms
3 clientes: m = 0.057ms, dp = 0.019ms 
4 clientes: m = 0.075ms, dp = 0.089ms 
5 clientes: m = 0.093ms, dp = 0.060ms
.
```

# Gráfico
![UDP x TCP](plot.png)
