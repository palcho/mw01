#include "libs.h"

#define LOCAL_PORT 54321
#define BUFFER_SIZE 1024
#define MAX_CLIENTS 5
#define MAX_PENDING MAX_CLIENTS

int listen_socket, client_socket;

void signal_handler (int signal_id);
int client_handler (void *client_socket);

int main (int argc, char **argv) {
	struct sockaddr_in local_address, remote_address;
	int  status;
	unsigned int addr_len;
	thrd_t thread;

	signal(SIGINT, signal_handler);

	// socket openning
	listen_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (listen_socket < 0) err("failed to open socket: %s\n", strerror(errno));

	// local address for socket
	local_address = (struct sockaddr_in) {
		.sin_family = AF_INET,
		.sin_addr.s_addr = htonl(INADDR_ANY),
		.sin_port = htons(LOCAL_PORT),
	};
	remote_address.sin_family = AF_INET;

	// socket binding to local address
	status = bind(listen_socket, (struct sockaddr *)&local_address, sizeof(struct sockaddr));
	if (status < 0) err("failed to bind socket to local address: %s\n", strerror(errno));

	// socket listening
	status = listen(listen_socket, MAX_PENDING);
	if (status < 0) err("failed to listen for socket connections: %s\n", strerror(errno));
	printf("server online\n");

	while (1) {
		// accept connections
		client_socket = accept(listen_socket, (struct sockaddr *)&remote_address, &addr_len);
		if (client_socket < 0) err("failed to accept connection: %s\n", strerror(errno));
		//printf("connection established\n");
		status = thrd_create(&thread, client_handler, (void *)(long)client_socket);
		if (status != thrd_success) err("failed to create thread: %s\n", strerror(errno));

	}

	status = close(listen_socket);
	if (status < 0) err("failed to close socket: %s\n", strerror(errno));
	printf("server offline\n");

	return 0;
}

void signal_handler (int signal_id) {
	printf("going down\n");
	int status = close(listen_socket);
	if (status < 0) err("failed to close listen socket: %s\n", strerror(errno));
	exit(0);
}

int client_handler (void *client_socket) {
	int msg_length, status;
	char buffer[BUFFER_SIZE];
	int sockt = (int)(long)client_socket;

	printf("socket %d opened\n", sockt);
	while (1) {
		// receives message from a client
		msg_length = recv(sockt, buffer, BUFFER_SIZE, 0);
		if (msg_length < 0) err("failed to receive message: %s\n", strerror(errno));
		if (msg_length == 0) break;
		// sends back a message
		msg_length = send(sockt, buffer, msg_length, 0);
		if (msg_length < 0) err("failed to send message: %s\n", strerror(errno));
	}

	status = close(sockt);
	if (status < 0) err("failed to close socket: %s\n", strerror(errno));
	printf("socket %d closed\n", sockt);

	return 0;
}
