#include "libs.h"

#define BUFFER_SIZE 1024
#define SERVER_IP "127.0.0.1"
#define SERVER_PORT 54321
#define NUM_ITERATIONS 10000

int sockt;

void signal_handler (int signal_id);

int main (int argc, char **argv) {
	struct sockaddr_in remote_address;
	char buffer[BUFFER_SIZE], message[] = "oi";
	int msg_length = strlen(message), status, waiting[NUM_ITERATIONS];
	struct timespec start, end;
	long avg_waiting = 0, std_deviation = 0; 

	signal(SIGINT, signal_handler);
	// socket openning
	sockt = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sockt < 0) err("failed to open socket: %s\n", strerror(errno));

	// server address
	remote_address = (struct sockaddr_in) {
		.sin_family = AF_INET,
		.sin_addr.s_addr = inet_addr(SERVER_IP),
		.sin_port = htons(SERVER_PORT),
	};

	// connect to server
	status = connect(sockt, (struct sockaddr *)&remote_address, sizeof(struct sockaddr));
	if (status < 0) err("failed to connect to server: %s\n", strerror(errno));
	printf("connected to the server\n");

	for (int i = 0; i < NUM_ITERATIONS; i++) {
		// sends message to the server
		clock_gettime(CLOCK_MONOTONIC_RAW, &start);
		status = send(sockt, message, msg_length, 0);
		if (status < 0) err("failed to send message: %s\n", strerror(errno));
		// receives message from the server
		status = recv(sockt, buffer, BUFFER_SIZE, 0);
		if (status < 0) err("failed to receive message: %s\n", strerror(errno));
		clock_gettime(CLOCK_MONOTONIC_RAW, &end);

		waiting[i] = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
		avg_waiting += waiting[i];
	}

	// statistics
	avg_waiting /= NUM_ITERATIONS;
	for (int i = 0, diff; i < NUM_ITERATIONS; i++) {
		diff = waiting[i] - avg_waiting;
		std_deviation += diff * diff;
	}
	std_deviation = (long)sqrt(std_deviation / NUM_ITERATIONS);

	printf("\naverage waiting time = %lu.%03lums\nstandard deviation   = %lu.%03lums\n",
		avg_waiting / 1000, avg_waiting % 1000, std_deviation / 1000, std_deviation % 1000);

	// close the socket
	status = close(sockt);
	if (status < 0) err("failed to close socket: %s\n", strerror(errno));

	return 0;
}

void signal_handler (int signal_id) {
	printf("going down\n");
	int status = close(sockt);
	if (status < 0) err("failed to close socket: %s\n", strerror(errno));
	exit(0);
}
