#ifndef LIBS_HEADER
#define LIBS_HEADER

#define _DEFAULT_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <threads.h>
#include <math.h>
// linux libraries
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
// sockets in linux libraries
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <arpa/inet.h>

#define err(...) do { fprintf(stderr, "error: " __VA_ARGS__); exit(-1); } while(0)

#endif // LIBS_HEADER
