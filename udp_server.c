#include "libs.h"

#define LOCAL_PORT 55555
#define BUFFER_SIZE 1024
#define TIMEOUT 100

int sockt;

void signal_handler (int signal_id);

int main (int argc, char **argv) {
	struct sockaddr_in local_address, remote_address;
	char buffer[BUFFER_SIZE];
	int msg_length, status;
	unsigned int addr_len;

	signal(SIGINT, signal_handler);

	// socket openning
	sockt = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sockt < 0) err("failed to open socket: %s\n", strerror(errno));

	// local address for socket
	local_address.sin_family = AF_INET;
	local_address.sin_addr.s_addr = htonl(INADDR_ANY);
	local_address.sin_port = htons(LOCAL_PORT);
	remote_address.sin_family = AF_INET;

	// socket binding to local address
	status = bind(sockt, (struct sockaddr *)&local_address, sizeof(local_address));
	if (status < 0) err("failed to bind socket to local address: %s\n", strerror(errno));
	printf("server online\n");

	while (1) {
		// receives message from a client
		msg_length = recvfrom(sockt, buffer, BUFFER_SIZE, 0, (struct sockaddr *)&remote_address, &addr_len);
		if (msg_length < 0) err("failed to receive message: %s\n", strerror(errno));

		// sends a message back
		msg_length = sendto(sockt, buffer, msg_length, 0, (struct sockaddr *)&remote_address, addr_len);
		if (msg_length < 0) err("failed to send message: %s\n", strerror(errno));
	}

	status = close(sockt);
	if (status < 0) err("failed to close socket: %s\n", strerror(errno));
	printf("server offline\n"); 

	return 0;
}

void signal_handler (int signal_id) {
	printf("going down\n");
	int status = close(sockt);
	if (status < 0) err("failed to close listen socket: %s\n", strerror(errno));
	exit(0);
}
