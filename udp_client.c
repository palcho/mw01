#include "libs.h"

#define BUFFER_SIZE 1024
#define SERVER_IP "127.0.0.1"
#define SERVER_PORT 55555
#define NUM_ITERATIONS 10000

int sockt;

void signal_handler (int signal_id);

int main (int argc, char **argv) {
	struct sockaddr_in local_address, remote_address;
	char buffer[BUFFER_SIZE], message[] = "oi";
	unsigned int addr_len = sizeof(remote_address);
	int msg_length = strlen(message), status, waiting[NUM_ITERATIONS];
	struct timespec start, end;
	long avg_waiting = 0, std_deviation = 0;

	signal(SIGINT, signal_handler);
	// socket openning
	sockt = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sockt < 0) err("failed to open socket: %s\n", strerror(errno));

	// binds the socket to a port
	local_address.sin_family = AF_INET;
	local_address.sin_addr.s_addr = htonl(INADDR_ANY);
	local_address.sin_port = 0; // port chosen by the OS

	status = bind(sockt, (struct sockaddr *)&local_address, sizeof(local_address));
	if (status < 0) err("failed to bind socket: %s\n", strerror(errno));
	printf("client ready\n");

	// server address
	remote_address.sin_family = AF_INET;
	remote_address.sin_addr.s_addr = inet_addr(SERVER_IP);
	remote_address.sin_port = htons(SERVER_PORT); // chosen by the OS

	for (int i = 0; i < NUM_ITERATIONS; i++) {
		// send message
		clock_gettime(CLOCK_MONOTONIC_RAW, &start);
		status = sendto(sockt, message, msg_length, 0, (struct sockaddr *)&remote_address, addr_len);
		if (status < 0) err("failed to send message: %s\n", strerror(errno));

		// receive message back
		status = recvfrom(sockt, buffer, BUFFER_SIZE, 0, (struct sockaddr *)&remote_address, &addr_len);
		if (status < 0) err("failed to receive message: %s\n", strerror(errno));
		clock_gettime(CLOCK_MONOTONIC_RAW, &end);

		waiting[i] = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
		avg_waiting += waiting[i];
	}

	avg_waiting /= NUM_ITERATIONS;
	for (int i = 0, diff; i < NUM_ITERATIONS; i++) {
		diff = waiting[i] - avg_waiting;
		std_deviation += diff * diff;
	}
	std_deviation = (long)sqrt(std_deviation / NUM_ITERATIONS);

	printf("\naverage waiting time = %lu.%03lums\nstandard deviation   = %lu.%03lums\n",
		avg_waiting / 1000, avg_waiting % 1000, std_deviation / 1000, std_deviation % 1000);

	// close the socket
	status = close(sockt);
	if (status < 0) err("failed to close socket: %s\n", strerror(errno));

	return 0;
}

void signal_handler (int signal_id) {
	printf("going down\n");
	int status = close(sockt);
	if (status < 0) err("failed to close listen socket: %s\n", strerror(errno));
	exit(0);
}
