EXE = udp_server tcp_server udp_client tcp_client
.PHONY: clean

compile: $(EXE)

%: %.c libs.h
	gcc $< -Wall -O3 -flto -pthread -lm -o $@

clean:
	-rm -f $(EXE)
